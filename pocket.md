# Getting PebblePocket up and running

## 1. Install [Java](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

We need Java to be able to build an Android app, so you can [Download latest Java (JRE & JDK)](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) and install it on your machine.

*Note: At the moment of writing this guide latest version of Java is **8u201***

---

## 2. Install [Android Studio](https://developer.android.com/studio/index.html)

---

## 3. Install Android SDK Platform and Platform-tools

- **Open Android SDK Manager.**

    Open Android Studio > Tools > SDK Manager.

    *If above didn't work for you, then find Android SDK Manager binary and run it with double click. You can find Android SDK Manager here: `C:\Users\USERNAME\AppData\Local\Android\sdk` the binary file should be named `android`.*

- **In Android SDK Manager install these things:**

  - Latest **Android SDK Tools** (v29.0.0)*
  - Latest **Android SDK Platform-tools** (24.0.2)*
  - **Android SDK Build-tools (23.0.3)**
  - **SDK Platform 26** (Android 6.0 API 23)
  - **Google USB Driver**

The USB driver is used if you want to debug on a device.

---

## 4. Install [NodeJS](https://nodejs.org/)

*Note: PebblePocket builds correctly on Node version 10.15.3 and NPM version 6.4.1. Newer versions of Node and NPM should work fine. For future major releases you may need to check with other developers before installing / running cordova and try to build PebblePocket.*

---

## 4a. Additional Step for macOS

*Ensure you have XCode installed from the AppStore, you may need business support to help with this.*

---

## 5. Prepare Directories On Your Machine

We recommend you to create a dedicated directory for all front-end projects, you can call it e.g. *PebbleProjects*.

|-- PebbleProjects/

This folder will contain PebblePocket as well as other front-end repositories (in case you have any). By the end of this guide you should have a structure like this.

|-- PebbleProjects/
|   |-- mobile/

---

## 6. Clone [PebblePocket Repository](https://bitbucket.org/pebblelearning/mobile)

---

## 7. Authenticate with PebblePad NPM

- Open console and run the following command

```bash
npm adduser --registry  https://awseu-bld03p.pebblepad.co.uk
```

*Note: credentials for PebblePad NPM are your [TeamCity](https://build.pebblepad.co.uk) user credentials*

---

## 8. Install PebblePocket Dependencies

- Open console and navigate to PebblePocket directory.

- Run `npm ci` to install dependencies.

- Run `npx cordova prepare`

    *Note: This command will install all necessary platforms (Android/iOS (only on macOS)) and all cordova plugins required by PebblePocket.*

- Verify the tests run `npm run test`

For the Android project, create a gradle.properties file in platforms/android and add the following line

```bash

#!

org.gradle.jvmargs=-Xmx2048M
```

This will reduce build time from roughly 12 seconds to 2 seconds.

---

## 9. Build

Now you have all the dependencies and platforms in place, time to Pocket building

Open a console window and navigate to the Pocket directory, and run the following command

```bash
npm run build
```

This builds pocket using PebblePack and provides all the necessary files for cordova to generate mobile builds.

By default this points pocket to the live servers, there is some parameters you can pass to change this.

```bash
npm run build -- --config=<app.config(default)|CONFIG_NAME>
```

app.config is the default config which points to the live servers, it's possible here to provide a `config` argument:

```bash
//app.pebbletest.co.uk
npm run build -- --config=taqas.app.config

//apptest.pebblepad.com
npm run build -- --config=testvm.app.config
```

It is also possible to duplicate one of these files, and modify it to your own needs for local development if you wish. The configs are stored in `/src/config/`

Once you've built for the whichever server you want, Now run

```bash
npm run mobilify
```

This runs a cordova build, and generates an APK by default, mobilify provides a couple of options

```bash
npm run mobilify [--] [--platform=<android(default)|ios|browser>] [--run]
```

_Note: the [--] is required to pass additional arguments through to the internal command_
So if you wanted to build for Android and run on the device currently plugged into your machine the command would be

```bash
npm run mobilify -- --platform=android --run
```

### Trying it out

Lets get it on a device! Go grab one of the Android testing devices.

**Ensure the USB debugging option is enabled on the device.**

**Ensure you have the Google USB Driver installed, listed in Android SDK Platform step.**

- Open the 'Settings' app.
- (Only on Android 8.0 or higher) Select 'System'.
- Scroll to the bottom and select 'About phone'.
- Scroll to the bottom and tap Build number 7 times.
- Return to the previous screen to find Developer options near the bottom.
- Open developer options
- Scroll down the list and find and enable `USB Debugging`

Plug the device into your machine via USB, and open a console window, enter the following commands

```bash
adb start-server
adb devices
```

Ensure your a device is listed, if not double check you have the Google USB Driver installed.

Open a console window and navigate to the Pocket directory, run the command stated previously

```bash
npm run mobilify -- --platform=android --run
```

Once cordova has done it's stuff, Pocket should pop up on the device.

---

## 10. Production

## How to release an Android App via Google Play Developer Console

Request the following with your *Line Manager*:

- Access to the Android signing `.keystore` and `release-signing.properties` files.

- Setup a Google Play Developer Console account with the PebblePad Org.


### Build release APK

- Create dedicated branch for the new app version *(e.g.: 1.1.9)*. Switch to that branch and then carry on.

- Open mobile app cordova config: `mobile/src/config.xml`

- Set `version` number to match new version *(e.g.: 1.1.8 -> 1.1.9)*.

- Increment the `android-versionCode` by 1.

- Open the mobile app config json file: `mobile/src/config/app.config.json`.

- Set the `appVersion` to match new version *(e.g.: 1.1.8 -> 1.1.9)*. **(This needed for in-app version number, which is visible on the settings page)**

- Open terminal and navigate to PebblePocket folder.

- Compile release APK via the npm command:

   ```bash
   npm run mobilify -- --platform=android --production --keystore=PATH_TO_.KEYSTORE_FILE  --storePassword=STORE_PASSWORD --password=PASSWORD
   ```

   *(note: Replace STORE_PASSWORD and PASSWORD with the values found in `release-signing.properties`)*

- After that you can find release APK in:
`mobile/platforms/android/build/outputs/apk/android-release.apk`

### Upload release APK to Google Play

- Login into [Google Play Developer Console](https://play.google.com/apps/publish/) (If you are authorised to do this, you will have your own account details)

- Select **PebblePocket app** from the apps list.

- Go to **APK** -> **Beta Testing** -> **Upload New APK to Beta**

- Then upload new app version APK via **Upload New APK to Beta**

- Populate the 'Release Notes'.

- After app is published into Beta list and you are ready to release it to all users you have to do this:

   1. Scroll down to the list of APKs and click **Promote to Production** next to the APK you want to release.
   2. Go to **Production** tab on the top.
   3. Check current release version if it's the one you want to see.

---

## How to release an iOS app via iTunes Connect?

Request the following with your *Line Manager*:

- An iTunesConnect Developer account linked with the PebblePad Org.

- Access to the Provisional profile / Developer signing certificates on the PebblePad Org.

---

### Build release IPA

- Create dedicated branch for the new app version *(e.g.: 1.1.9)*. Switch to that branch and then carry on.

- Open mobile app cordova config: `mobile/src/config.xml`

- Set `version` number to match new version *(e.g.: 1.1.8 -> 1.1.9)*.

- Increment the `android-versionCode` by 1.

- Open the mobile app config json file: `mobile/src/config/app.config.json`.

- Set the `appVersion` to match new version *(e.g.: 1.1.8 -> 1.1.9)*. **(This needed for in-app version number, which is visible on the settings page)**

- Open terminal, navigate to mobile app repository folder and run cordova command:

```bash
    npm run mobilify -- --platform=ios
```

- Open Xcode project file in Xcode: `mobile/platforms/ios/Pebble Pocket.xcodeproj`.
- Make sure all settings and version number are correct.

- Open **General Settings**

- If you have not already, add your iTunesConnect account into Xcode. This can be done via the **Team** list under the **Signing** section.

- Under **Signing**, tick the `Automatically manage signing` option.

- Open the **Project Settings** under `File -> Project Settings`, then change the **Build System** to `Legacy Build System`.

- In the top menu select **Generic iOS Device** as the build destination.
- Product -> Archive
- Validate build.
- Upload app to app store.

### Upload release APK to iTunes

- Login into [iTunes Connect](https://itunesconnect.apple.com/itc/static/login)

- Go to the **PebblePocket** app page.

- Select your build.

- Populate the 'Release Notes'.

- Once everything has been checked over, click **Submit for review**.