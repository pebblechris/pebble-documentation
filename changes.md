## Question ##

As a plucky developer, I am curious about the changes which have gone on with replacement the build process for `RequireJs`/`Gulp`. What things should I be aware of?

------
## Answer ##

Quite a few changes have had to occur in order to modernise our Frontend codebase, help improve code quality and prepare us for the TypeScript future!

---

## Noticeable Changes ##

*Note: If you want to skip the explanations of what has changed, jump to the "This is cool and all, but what does this mean for me?" section.*


### Ditched RequireJs ###

`RequireJs` is somewhat deprecated and does nothing to help prevent circular dependencies or Developers missing required dependencies. It forces developers to have to do additional setup and maintenance for code and Unit tests.

We have entirely ditched RequireJs and replaced it with ES6 `import` and `export` syntax. This syntax is a standardised specification and works great with TypeScript.

You might be asking "how can we use ES6 import / export, when we have to support IE11?". The answer to that is the build process handles it. One of the tasks in the build process transpiles the ES6 code into ES5 code bundles with support for lazy-load wrapping. This allows you the developer to care little about Browser support and instead focus on writing good, clean, robust code!

This does come at the cost of now having to wait for a build completion in order to load a SPA. You cannot simply immediately reload a page when you make changes. A full build is around 30s, an incremental watched build is typically between 0.5s and 3s.

⁣

### Enforced AngularJs Dependency Injection strings system wide ###

A lot of older files which make use of `AngularJs`, rely on 'magic' argument name dependency injection:

```JavaScript
angularAMD.directive("myDirective", function($http, $q, myService) {
    return {
        restrict: "E",
        scope: {},
        controller: function($scope, myOtherService) {}.
        link: function(scope, element, attrs) {}
    }
});
```

This prevented us from being able to minify our code. During a minification process arguments and variables will often be shortened to single letters (i.e. `myService` would become `c`). 
As `AngularJs` relied on the argument name to find and inject the requested item, it would always fail on minified code.

All `AngularJs` code now has and must have explicit injector strings:

```JavaScript
//Directive
angularAMD.directive("myDirective", ["$http", "$q", "myService", function($http, $q, myService) {
    return {
        restrict: "E",
        scope: {},
        controller: ["$scope", "myOtherService", function($scope, myOtherService) {}],
        link: function(scope, element, attrs) {}
    }
}]);

//Component - standard ES5 syntax
MyComponent.$inject = ["myService"];
function MyComponent(myService) {}
```

*"Why do we want to minify our code?"* you ask. The reasons are simple. Reduce network request size and improve loading time of SPAs.

⁣

### AngularJs component templates ###

To improve readability, drastically decrease the number of network requests, reduce maintenance and allow us to easily minify our HTML templates for `AngularJs` components, we have removed the need to specify a `templateUrl` using `$sce` and `baseUrlsFactory`. 

 ```JavaScript
angular.component("myComponent", {
    templateUrl: ['$sce', 'baseUrlsFactory', function($sce, baseUrlsFactory) {
        return $sce.getTrustedResourceUrl(baseUrlsFactory.shared_component_base_url + 'myComponent/my-component.html');
    }]
});
```

Now HTML templates can be imported using an ES6 import statement and a `template` property declared on the `AngularJs` component.

```JavaScript
//Relative path to HTML from the current JS file
import template from "./my-component.html";

angular.component("myComponent", {
    template: template, //TIDY!
});
```

It is worth noting that this is not a standard JavaScript ES6 import! When the build process is processing import statements and it comes across a `.html` import, it will grab the contents of the HTML file and create a JavaScript variable for the string contents.

⁣

### Unit tests ###

With `RequireJs` out of the picture, we finally had the opportunity to ditch the flaky `KarmaJs` test runner and `Jasmine`. We have moved over to [Jest][1]!

`Jest` ensures a more stable environment and clean test runs (no test pollution like you get with KarmaJs). Unlike `KarmaJs`, `Jest` does not run in a browser, instead it uses a virtual DOM with a regular NodeJs process. 

The testing framework `Jasmine` was fully replaced by `Jest` as well. `Jest` is both a test runner and a framework. Thankfully it supports the majority of the `Jasmine` syntax, allowing us to have a smoother transition.

⁣

### SCSS/CSS vendor prefixing ###

We were relying on SCSS mixins to apply vendor prefixes for cross-browser support of various CSS properties (e.g. Flexbox). Not only did that end up cluttering our code a bit, but the usage of mixins was often forgotten by Developers when they were writing SCSS.

As part of the SCSS/CSS compilation, the majority of the vendor prefixes will be automatically applied for us via an [AutoPrefixer][5]. This means that we can ditch the use of common vendor prefix mixins and write cleaner SCSS/CSS!

⁣

### Repo Dependencies ###

With the `Gulp` and `RequireJs` setup, a SPA had to know the path of `PebblePadComponents`, regardless of whether or not it was a server build, local production build or development build. This made the build / config process quite annoying to deal with (especially when Devs had different environment setups).

We now normalise all dependencies through `npm`. `PebblePadComponents` is registered as a `npm` package. This means that for server and local production builds, all that is needed to setup a SPA like Plus is to execute `npm ci` in the project directory!

For development builds (working on `PebblePadComponents` and a SPA at the same time), it requires a `npm link` between your SPA project and the `PebblePadComponents` directory. 

Fret not! We have streamlined the process by making our new build tool ([PebblePack][2]) have the ability to automatically link your local development projects for you via configuration properties - see the `localDevelopmentPackages` in the config in the [PebblePack readme][2] for more info.

Every time a `PebblePadComponents` branch is merged into `master`, a new version of the `PebblePadComponents` package will be made available (manual process as of Apr 2019). For feature branches, pre-release tags will be used. More info on `npm` packages and future development will come later!

Example for Feature branch updating:

-  Update `PebblePadComponents`'s `package.json` version to match the feature (e.g. `1.0.0-feedback`).

- Publish the new version of `PebblePadComponents` via `npm publish`

- Update SPA's `package.json` to use new version (e.g. `"@pebblepad/components": "1.0.0-feedback"`)

- Run an `npm install`.

- Commit, push and wait for builds!

For an update to the same pre-release/feature package, typically the best choice so to update the version suffix with a number - `1.0.0-feedback.1` to `1.0.0-feedback.2`.
   


⁣

### The minimum supported NodeJs version has been raised ###

Developers are now required to have a stable NodeJs `10.X.X` version installed. Versions less than 10, will not work with the new build process.

Read the NodeJs section in the *["How do I setup PebblePad for development on my machine?"][11]* guide for extra information!

⁣

### We can now write ES6 and TypeScript ###

Without a build process in place, we were trapped using ES5. As we now internally leverage on well-known and *reliable* JS bundlers/compilers like [WebPack][6], [Rollup][7], [Babel][8], [TypeScript][9], we can write **most** ES6 without worrying about browser support. 

There are some limitations with ES6, such as proxies, variable dynamic imports, .etc. For a list please read through our [ES6 guide][10].

We have support for TypeScript, but it **will not and never will** be supported in `PebblePadComponents`! Support may eventually come for the SPAs, but for now they are also **not** supported. ES6 can however be used in `PebblePadComponents` and SPAs.
**TypeScript is only supported and enforced with new repositories and private `npm` packages we publish**. 

Guides will be made available for TypeScript and `npm` packages in the future.

⁣

---

## This is cool and all, but what does this mean for me? ##

### Setup Changes ###

- You add are required to have a stable version of NodeJs `10.X.X` installed.

   > *Note: If you need to be able to build a `Gulp`/`RequireJs` SPA, you will need to revert back to back to a NodeJs `8.X.X` version.*

- You are required to setup a `npm` user in all Frontend project repos, in order to pull PebblePad's private packages. See the "*Setup npm account*" section in the [setup guide][11] for more info!

- Your `config.js` files need updating to point to a new `PebblePadComponents` location:
   ```JavaScript
   getSharedComponentBaseUrl: function () {
       return "https://<my-pc>/<spa-name>/node_modules/@pebblepad/components/";
   },
   ```

- You **must** update your the `Physical path` in IIS for each SPA to point to the `dist` folder instead of `app`.

   For example `C:\pebble\frontend\plus\app` would be changed to `C:\pebble\frontend\plus\dist`.

   To make the changes, open IIS, expand `Sites`, then `Default Web Site`. For each SPA, click on the site in the list and then on the far right click `Basic Settings`. That will launch a prompt which allows you to change the `Physical path`.

   Alternatively the `setuppp.ps1` script could be used again. See the "*Setting up the apps*" section in the [setup guide][11] for where to find it and how to run it.

⁣

### Local build changes ###

- You **must** run a build or watch command and wait for completion in order to load a SPA! You cannot immediately reload a page when you make changes.

   `npm run watch` OR `npm run build`

  > *Note: watch completion is usually fairly quick. By the time you get to the browser and trigger a refresh, the watch build made have already completed.*

- You **must** link `PebblePadComponents` when locally developing on it! See *"Example setup for Plus and PebblePadComponents"* in the [development guide][3]!

- You **should** run unit tests via the `npm test` command.

   > *Note: `npm run karma-tests` is still supported as a deprecated command.*
⁣

### Server build changes ###
- You **must** push a feature branch pre-release version of `PebblePadComponent` and update the `package.json` in the relevant SPAs in order for a feature branch to successfully build on TeamCity.

- Server **must** have a stable version of NodeJs `10.X.X` installed, as well as a stable version of NodeJs `8.X.X`, with the ability to switch between which NodeJs version is used depending on if it is a new build or 'old' `RequireJs` build.

   > *Note: TeamCity build server now makes use of a 'if gulp exists'  and changes the PATH for NodeJs*.

- Server **must** setup a `npm` user in all Frontend project repo directories, in order to pull PebblePad's private packages.

⁣

### Deployment changes ###

- Deployment server **must** be able to change the `getSharedComponentBaseUrl` in `config.js`  depending on if a SPA project build is a new or 'old' `RequireJs`/`Gulp` build. 

   Using an environment variable, the path for a 'new' build should be updated to point to a new `PebblePadComponents` location:
   ```JavaScript
   getSharedComponentBaseUrl: function () {
       return "https://<server>/<spa-name>/node_modules/@pebblepad/components/";
   },
   ```

   Releases onto non-automated servers, may require manual changes to `config.js` (release tasks).

⁣

### Code changes ###

- No longer use the `RequireJs` syntax:
   ```JavaScript
   define("myThing", ["myDependency"], function() {
       function myFunc() {}
       return myFunc;
   });
   ```

   **instead** ES6 import/exports are used:

   ```JavaScript
   import { myDependency } from "../myDependency";

   export function myFunc(){}
   //OR!
   function myFunc(){}
   export { myFunc };
   ```
   
⁣

- No longer use `templateUrl` to load `AngularJs` component HTML templates:
   ```JavaScript
   angular.component("myComponent", {
        bindings: {},
        templateUrl: ['$sce', 'baseUrlsFactory', function($sce, baseUrlsFactory) {
            return $sce.getTrustedResourceUrl(baseUrlsFactory.shared_component_base_url + 'myComponent/my-component.html');
        }],
        controller: MyComponent
   });
   ```
   **instead** import a HTML template and use the `template` property

   ```JavaScript
   //Top of file next to any other imports
   import template from "./my-component.html";

   angular.component("myComponent", {
        bindings: {},
        template: template,
        controller: MyComponent
   });
   ```

⁣
⁣
- No longer required to register new JS files in `main.js` or `karma.conf`. **Instead** ES6 imports handle everything!

- No longer use `module()` in Unit tests
   ```JavaScript
   beforeEach(module(function(){
        /* body */ 
   }));
   ```
   **instead** use `angular.mock.module()`
   ```JavaScript
   beforeEach(angular.mock.module(() => { 
       /* body */ 
   }));
   ``` 

⁣
- No longer use `it()` in Unit tests
   ```JavaScript
   it("should do something!", function() {

   });
   ```
   **instead** use `test()`
   ```JavaScript
   test("it should do something!", () => {

   });
   ```
   
   > *Note: `it()` is still supported as a legacy method.*

⁣


- No longer use `Jasmine` specific methods in Unit tests

   ```JavaScript
   //e.g.
   jasmine.createSpy("mySpy");
   jasmine.objectContaining();
   ```
   **instead** use Jest methods
   ```JavaScript
   jest.fn();
   expect.objectContaining();
   ```

   Check out the [Jest API][4] for more available methods!

   > *Note: Majority of `Jasmine` methods still work for legacy support.*

⁣


- No longer have to use SCSS vendor prefix mixins for common CSS properties
   ```CSS
   .my-rule {
      @include display(flex);
      @include box-sizing(border-box);
   }
   ```
   **instead** the property can cleanly be declared.
   ```CSS
   .my-rule {
      display: flex;
      box-sizing: border-box;
   }
   ```

[1]: https://jestjs.io/
[2]: https://awseu-bld03p.pebblepad.co.uk/#/detail/@pebblepad/pebblepack
[3]: TODO---SO
[4]: https://jestjs.io/docs/en/api
[5]: https://github.com/postcss/autoprefixer
[6]: https://webpack.js.org/
[7]: https://github.com/rollup/rollup
[8]: https://babeljs.io/
[9]: https://www.typescriptlang.org/
[10]: TODO---ES6
[11]: https://stackoverflow.com/c/pebblepad/questions/204