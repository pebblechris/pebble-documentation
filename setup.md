## Account prep ##

You **must** have an account on https://bitbucket.org/ (**BitBucket**) and have access to all of the PebblePad repositories via it. 

You **must** have an account on https://build.pebblepad.co.uk/ (**TeamCity**). 

## Software Prep ##

Ensure you have the latest non-prerelease version of Visual Studio Professional installed. This usually comes preinstalled on your machine from Business Support.

**Set Visual Studio to always run as Administrator**

Note: We'll be setting the PebblePad applications to run in IIS later. The IIS service runs as a different user and isolated session to you, so in order for Visual Studio to attach to an IIS application as a debugger it needs Administrator privileges.

Right click on your shortcut to Visual Studio 2017 and go to properties:

 - If it is in your taskbar, right click the Visual Studio icon and then right click "Visual Studio 2017" in the list and select properties
 - If it is in Start menu, right click it and select "Open file location", and right click the shortcut there and select "Properties"

Click the "Advanced" button and tick "Run as Administrator", then click OK on all dialogs. 

**.NET SDK**

Make sure the latest .NET SDK and Target Pack is installed. To do this go to Start and type "Visual Studio Installer" and Enter.

Next to your installation of Visual Studio, select the "More" dropdown and select "Modify", then on the modifying screen, go to the "Individual Components" tab at the top. 

Tick all of the .NET Framework SDK/Targeting 4.*.* boxes up to and including the latest (at time of writing is 4.7.2), and click the "Modify" button at the bottom right.

**Git**

Ensure you have Git for Windows installed from https://gitforwindows.org/, or if your chosen console is Windows Subsystem for Linux (bash) then install it via `sudo apt install git`. 

**Robo 3T**

If it's not installed already, install Robo 3T (not Studio 3T).
As we are currently on mongodb 2 atm you'll need to download an older version as the current version (1.3 at time of writing) doesn't support it.

Go to https://github.com/Studio3T/robomongo/releases/tag/v1.2.0-beta and download and install it.

Open it and click the blue "Create" link in the top-left corner of the dialog box. Name it `appdev` and set the address as `mongodb`. Click Save. 

**NodeJS**

Download and install the latest version of NodeJS **10** from https://nodejs.org/en/download/releases/, or if your chosen console is Windows Subsystem for Linux (bash) then install it via the commands available here https://github.com/nodesource/distributions/blob/master/README.md.

**Windows Features**

Open Start and type "appwiz.cpl" and press Enter. On the left hand side select "Turn Windows features on or off". In this list ensure the following are turned on:

 - Internet Information Services/
    - Web Management Tools
        - IIS Management Console
    - World Wide Web Services
        - Application Development Features
            - .NET Extensibility 4.7
            - ASP.NET 4.7
        - Common HTTP Features
            - Default Document
            - Static Content
        - Health and Diagnostics
            - HTTP Logging
        - Performance Features
             - Static Content Compression
 - Microsoft Message Queue (MSMQ) Server (select this not it's child so the tickbox is solid)

## Cloning ##

**Setting up your SSH**

In your chosen console (Powershell, WSL etc...), follow the instructions at https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html to setup SSH. 

**Cloning the repositories**

Go to BitBucket at https://bitbucket.org/dashboard/repositories and clone the following repositories via SSH into somewhere on your D:\ drive. 

 - v3backend
 - pebblepadcomponents
 - plus
 - atlas
 - mobile (Pebble Pocket)
 - flourish
 - admissions
 - misc (optional)

To clone a repository, find the repository on BitBucket and click on the `Clone` button at the top right. Make sure `SSH` is selected in the new dialog, then copy and paste the command in to your console (`cd`ing into where you want to store your PebblePad code beforehand).

## First Backend Build ##

Open Visual Studio (as an Administrator) and open "All.sln" from the v3backend repository. 

Visual Studio may prompt you about certain projects requiring SQL Server Express. Tick the "Don't ask me for every project in this solution" option and click OK.

Click through the Solution Explorer and check all of the projects seem to have loaded OK. If all is when then go to the "BUILD" menu at the top of Visual Studio, and select "Build Solution".

Visual Studio may ask you for a username and password so it can restore packages from our TeamCity nuget feed - type in your username and password for TeamCity (https://build.pebblepad.co.uk). If you think you've typed this in wrong follow this to change it: https://stackoverflow.com/c/pebblepad/questions/150.

## Setting up Queues ##

Go to Start and type in "Component Services" and press Enter. In the new window, on the left hand side navigate to "Components Services" / "Computers" / "My Computer" / "Distributed Transaction Coordinator". Right click "Local DTC" and select "Properties".

Go to the "Security" tab and tick the following:

- Network DTC Access
- Allow Remote Clients
- Allow Remote Administration
- Allow Inbound
- Allow Outbound
- No Authentication Required

Click OK.

Now open **Powershell** as an **Administrator** and navigate in to your backend repository directory once again and run the following command:

`AppServices\AppServices.Application\UpdateScripts\EnsureAllQueues.ps1`

Note: Running this command is optional since the `setuppp.ps1` script later will do this for you.

## Setting up IIS ##

Note: We use IIS over IIS express to support SSL, have a more live-like environment, and have compatibility with our XSRF protection.

**Setting up SSL**

Follow the instructions at [How do I request a TLS certificate from the Pebble Learning Certification authority?][1] including the part titled "Update IIS bindings for IIS".

**Setting up the apps**

Open **PowerShell** as an **Admin** and navigate in to your backend repository directory and execute the following command:

`AppServices\AppServices.Application\UpdateScripts\setuppp.ps1`

It will ask you a series of questions:

 1. It will ask you for your **domain** username and password, type your username in full form like `PEBBLELEARNING\andrewj`
 2. It will ask you for your Backend root directory, put in the path to your cloned v3backend repository, for example - assuming you cloned v3backend inside of `D:\PebblePad` - type in `D:\PebblePad\v3backend`.
 3. It will ask your for your spa root directory, put in the path to the folder that *contains* your clones frontend repositories, for example - assuming you cloned plus, pebblepadcomponents etc. inside of D:\PebblePad - type in `D:\PebblePad\`
 4. It will ask for your root web URL - put in the URL to your PC (using FQDN) without an ending slash, for example `https://andrewj-pc.pebblelearning.lan` 
 5. It will ask if you want to set IIS as debugging platform in Visual Studio - put in `$true`

It should then go through and setup all the PebblePad sites in your IIS.

**Setting up your InstallDto**

 - Open Robo3T and connect to AppDev. 
 - On the left navigate into "V3Dev" > "Collections" and double click "InstallDto". 
 - On the right hand side, right click "root" and select "Copy JSON".
 - On the right hand side, right click an empty space and select "Insert Document"
 - Delete the `{` and `}` that it puts in for you and paste the JSON
 - Edit the `_id` at the top to be your name (no spaces), for example: `"_id" : "andrewj",`
 - Edit all of the URLs to match your machine, for example `"GatewaysUrl" : "https://appdev/atlas/"` would become `"GatewaysUrl" : "https://andrewj-pc.pebblelearning.lan/atlas/"`
 - Replace the `"ComponentsUrl"` entry to `https://<your-pc>/plus/node_modules/@pebblepad/components/`. For example on `andrewj-pc` it would be `https://andrewj-pc/plus/node_modules/@pebblepad/components/`
 - Save the document (if it errors, look for `"` or `,` characters you may have accidentally removed)

Now open **Powershell** and navigate in to your backend repository directory once again and run the following command:

`AppServices\AppServices.Application\UpdateScripts\ChangeInstallDto.ps1 <your_install_dto_id_here>` 

Replacing `<your_install_dto_id_here>` with the Id you put in your InstallDto earlier, for example: 

`AppServices\AppServices.Application\UpdateScripts\ChangeInstallDto.ps1 andrewj` 

## Starting up Backend ##

Open the All solution in Visual Studio again. Right click on the Solution at the top of the Solution Explorer (`"Solution 'All' (82 projects)"`) and select "Set StartUp projects...".

In the new dialog we'll choose which projects to launch when starting a debugging session. Select "Start Multiple Projects" and change the 'Action' on the following projects to "Start":

 - Admin.Web
 - AdmissionsInterface
 - Api
 - AssetStore.Application
 - DataIsland (this app requires a bit more setup)
 - EmailServices
 - FileUploadService
 - Flourish.Application
 - Flourish.Web
 - Gateways
 - LoginService
 - PathwayServices
 - PlusInterface
 - Portfolio.Web
 - QueueServices 
 - InstallCollator (optional - for PebblePocket)
 - WebStore
 - ATLAS.Api

Note: The rest of the projects are libraries and are used by one or more of these apps.

Click OK, and click START (or F5)!

The backend services should start up, and you should be able to go to your MVC frontend apps like /admin (for example https://andrewj-pc.pebblelearning.lan/admin).

## Setting up frontend 

**PebblePad Components**

First taks is to enable access to our private `npm` server and the packages on it.
Navigate to your pebblepad components repository in your console and execute `npm adduser --registry https://awseu-bld03p.pebblepad.co.uk`. 

Follow the onscreen prompts. When prompted to enter a username and password, use your TeamCity account credentials (https://build.pebblepad.co.uk/).

Afterwards in the console, execute `npm ci`.

**Apps**

Repeat the rest of this section for each SPA application (plus, admissions, atlas, and flourish).

**Setup npm account**

Navigate to the SPA app's repository in your console and execute `npm adduser --registry https://awseu-bld03p.pebblepad.co.uk`. 

Follow the onscreen prompts. When prompted to enter a username and password, use your TeamCity account credentials (https://build.pebblepad.co.uk/).

**npm ci (npm install)**

Navigate to the SPA app's repository in your console and execute `npm ci`.

**API config**

Inside of the "app" folder take a copy of the "config-sample.js" file and call it "config.js". 

Open the file up and replace the following:

- Replace `{{components_url}}` with the web URL to your PebblePadComponents (with an ending `/`), which - following this guide - will be `https://<your-pc>/<app>/node_modules/@pebblepad/components/`. 

   For example: `return "https://andrewj-pc.pebblelearning.lan/plus/node_modules/@pebblepad/components/";`

- Replace `{{api_base_url}}` with the web URL to your app's API with an extra `/api` on the end, which - following this guide - will be:
    - for plus `https://<your-pc>/plusapi/api/`
    - for atlas `https://<your-pc>/atlasapi/api/`
    - for admissions `https://<your-pc>/admissionsapi/api/`
    - for flourish `https://<your-pc>/flourishapi/api/`

- Only for **Flourish**, replace `{{renderer_url}}` with the same as `{{api_base_url}}` but without the extra `/api/` at the end, for example `https://andrewj-pc.pebblelearning.lan/flourishapi/`.

- Replace {{sentry_project_url}} with the following:
    - for plus https://983574646bb74fa8ae9da762e325513a@sentry.pebblepad.co.uk/5
    - for spa-atlas https://3034c3da99f84e3db71039240702de01@sentry.pebblepad.co.uk/6
    - for flourish https://b23082b3a59c4b9887fbc39dd4a9a448@sentry.pebblepad.co.uk/7
    - for admissions https://b7b22e96adbe4f4691dcbc2436204a2a@sentry.pebblepad.co.uk/9


**First App Build**

Navigate to the SPA app's directory in your console and execute `npm run build`.


Your frontend SPA apps should now be setup! Give Plus a try at `https://<your-pc>/plus/`, for example `https://andrewj-pc.pebblelearning.lan/plus`. It should redirect you to login, then after logging in (with any of the same credentials you may have for https://appdev/) fully load the dashboard.

## Backend and Frontend unit tests ##

Now follow the instructions at the SO posts below to setup for running unit tests:

 - [Backend Unit Tests][2]
 - [Frontend Unit Tests][3]

## You're done! ##

Well done - it's been a long day. Take a break, make some tea, eat some snacks, revel in the fact that you're now ready to develop in PebblePad.


  [1]: https://stackoverflow.com/c/pebblepad/questions/111
  [2]: https://www.youtube.com/watch?v=dQw4w9WgXcQ
  [3]: https://www.youtube.com/watch?v=Y_IS_ccjkZE