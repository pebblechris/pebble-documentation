
### JavaScript Style Guide 
Forked from Airbnb's ES6 style guide and improved by PebblePad.


----
## Table of Contents


  [TOC]


----
## Types

  
 
### 1.1 Primitives
  When you access a primitive type you work directly on its value.

  - `string`
  - `number`
  - `boolean`
  - `null`
  - `undefined`
  - `symbol`


```javascript
  const foo = 1;
  let bar = foo;

  bar = 9;

  console.log(foo, bar); // => 1, 9
```

  - *Note: Symbols cannot be faithfully polyfilled, so they should not be used when targeting browsers/environments that don’t support them natively.*

  
 
### 1.2 Complex
   When you access a complex type you work on a reference to its value.

  - `object`
  - `array`
  - `function`

```javascript
  const foo = [1, 2];
  const bar = foo;

  bar[0] = 9;

  console.log(foo[0], bar[0]); // => 9, 9
```



----
## References

  
 
### 2.1 const
  Use `const` for all of your references; avoid using `var`. 

  > Why? This ensures that you can’t reassign your references, which can lead to bugs and difficult to comprehend code.

```javascript
  // bad
  var a = 1;
  var b = 2;

  // good
  const a = 1;
  const b = 2;
```

  
 
### 2.2 let
  If you must reassign references, use `let` instead of `var`. 

  > Why? `let` is block-scoped rather than function-scoped like `var`.

```javascript
  // bad
  var count = 1;
  if (true) {
    count += 1;
  }

  // good, use the let.
  let count = 1;
  if (true) {
    count += 1;
  }
```

  
 
### 2.3 Scoping
  Note that both `let` and `const` are block-scoped.

```javascript
  // const and let only exist in the blocks they are defined in.
  {
    let a = 1;
    const b = 1;
  }
  console.log(a); // ReferenceError
  console.log(b); // ReferenceError
```



----
## Objects

  
 
### 3.1 Literal syntax creation
  Use the literal syntax for object creation. 

```javascript
  // bad
  const item = new Object();

  // good
  const item = {};
```

  
 
### 3.2 Dynamic property names
  Use computed property names when creating objects with dynamic property names.

  > Why? They allow you to define all the properties of an object in one place.

```javascript

  function getKey(k) {
    return `key-${k}`;
  }

  // bad
  const obj = {
    id: 5,
    name: "San Francisco",
  };
  obj[getKey("enabled")] = true; //obj["key-enabled"]

  // good
  const obj = {
    id: 5,
    name: "San Francisco",
    [getKey("enabled")]: true, //obj["key-enabled"]
  };
```

  
 
### 3.3 Literal object methods
  Use object method shorthand. 

```javascript
  // bad
  const atom = {
    value: 1,

    addValue: function (value) {
      return atom.value + value;
    },
  };

  // good
  const atom = {
    value: 1,

    addValue(value) {
      return atom.value + value;
    },
  };
```

  
 
### 3.4 Avoid property declaration shorthand.
  Avoid property value shorthand. 

  > Why?  As JS has no typing safeguards, object property shorthand can easily lead to accidental property structure changes (renaming variables) and more easily introduce typos.

```javascript
  const lukeSkywalker = "Luke Skywalker";

  // avoid
  const obj = {
    lukeSkywalker,
  };

  // good
  const obj = {
    lukeSkywalker: lukeSkywalker,
  };
```

  
 
### 3.5 String named properties
  Only quote properties that are invalid identifiers. 

  > Why? In general we consider it subjectively easier to read. It improves syntax highlighting, and is also more easily optimized by many JS engines.

```javascript
  // bad
  const bad = {
    "foo": 3,
    "bar": 4,
    "data-blah": 5,
  };

  // good
  const good = {
    foo: 3,
    bar: 4,
    "data-blah": 5,
  };
```

  
 
### 3.6 Shallow-copy with properties omitted
  Prefer the object spread operator over [`Object.assign`](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Object/assign) to shallow-copy objects with certain properties omitted.

```javascript
  // very bad
  const original = { a: 1, b: 2 };
  const youThoughtYouWereGettingACopyButDidNot = Object.assign(original, { c: 3 }); // this mutates `original` ಠ_ಠ
  delete youThoughtYouWereGettingACopyButDidNot.a; // so does this

  // bad
  const original = { a: 1, b: 2 };
  const copy = Object.assign({}, original, { c: 3 }); // copy => { a: 1, b: 2, c: 3 }

  // good
  const original = { a: 1, b: 2 };
  const copy = { ...original, c: 3 }; // copy => { a: 1, b: 2, c: 3 }

  const { a, ...noA } = copy; // noA => { b: 2, c: 3 }
```



----
## Arrays

  
 
### 4.1 Construction
  Use the literal syntax for array creation. 

```javascript
  // bad
  const items = new Array();

  // good
  const items = [];
```

  
 
### 4.2 Adding to an Array
  Use [Array.push](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/push) instead of direct assignment to add items to an array.

```javascript
  const someStack = [];

  // bad
  someStack[someStack.length] = "abracadabra";

  // good
  someStack.push("abracadabra");
```

  
 
### 4.3 Copying
  Use array spreads `...` or `.slice` to copy arrays.

```javascript
  // bad
  const len = items.length;
  const itemsCopy = [];
  let i;

  for (i = 0; i < len; i += 1) {
    itemsCopy[i] = items[i];
  }

  // good
  const itemsCopy = [...items];

  // good
  const itemsCopy = items.slice();
```

  
  
 
### 4.4 Iterable object to Array 
  To convert an iterable object to an array, use spreads `...` or [`Array.from`](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/from).

```javascript
  const foo = document.querySelectorAll(".foo");

  // decent
  const nodes = Array.from(foo);

  // better
  const nodes = [...foo];
```

  
 
### 4.5 Array-like object to Array
  Use [`Array.from`](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/from) for converting an array-like object to an array.

```javascript
  const arrLike = { 0: "foo', 1: 'bar', 2: 'baz", length: 3 };

  // bad
  const arr = Array.prototype.slice.call(arrLike);

  // good
  const arr = Array.from(arrLike);
```

  
 
### 4.6 Mapping iterables to Array
  Use [`Array.from`](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/from) instead of spread `...` for mapping over iterables, because it avoids creating an intermediate array.

```javascript
  // bad
  const baz = [...foo].map(bar);

  // good
  const baz = Array.from(foo, bar);
```

  
 
### 4.7 Array method callback return values

  When using an array iteration method which can return a value (i.e. `.map` but not `.forEach`), always ensure the iteration method's callback contains return statements for all outcomes.
  

```javascript
  // good
  [1, 2, 3].map((x) => {
    const y = x + 1;
    return x * y;
  });

  // good
  [1, 2, 3].map(x => x + 1);

  // bad - no returned value means `acc` becomes undefined after the first iteration
  [[0, 1], [2, 3], [4, 5]].reduce((acc, item, index) => {
    const flatten = acc.concat(item);
  });

  // good
  [[0, 1], [2, 3], [4, 5]].reduce((acc, item, index) => {
    const flatten = acc.concat(item);
    return flatten;
  });

  // bad (unnecessary else)
  inbox.filter((msg) => {
    const { subject, author } = msg;
    if (subject === "Mockingbird") {
      return author === "Harper Lee";
    } else {
      return false;
    }
  });

  // good
  inbox.filter((msg) => {
    const { subject, author } = msg;
    if (subject === "Mockingbird") {
      return author === "Harper Lee";
    }

    return false;
  });
```



----
## Destructuring

  
 
### 5.1 Variable destructuring
  Consider using object destructuring when accessing and using multiple properties of an object, instead of declaring separate variables. 

```javascript
  // bad
  function getFullName(user) {
    const firstName = user.firstName;
    const lastName = user.lastName;

    return `${firstName} ${lastName}`;
  }

  // good
  function getFullName(user) {
    const { firstName, lastName } = user;
    return `${firstName} ${lastName}`;
  }

  // good
  function getFullName(user) {
    return `${user.firstName} ${user.lastName}`;
  }

  // good
  function getFullName({ firstName, lastName }) {
    return `${firstName} ${lastName}`;
  }
```

  
 
### 5.2 Array destructuring
  Use array destructuring over direct index access when working with **fixed** sized arrays and accessing indexes in order (i.e. 0, 1, 2, 3, .etc). 

```javascript
  const arr = [1, 2, 3, 4];

  // bad
  const first = arr[0];
  const second = arr[1];

  // good
  const [first, second] = arr;
```


----
## Strings

  
 
### 6.1 Syntax
  Use double quotes `""` for strings. 

```javascript
  // bad
  const name = 'Capt. Janeway';

  // bad - template literals should contain interpolation or newlines
  const name = `Capt. Janeway`;

  // good
  const name = "Capt. Janeway";

```

  
 
### 6.2 Long strings
  Strings that cause the line to go over 100 characters should not be written across multiple lines using string concatenation.

  > Why? Broken strings are painful to work with and make code less searchable.

```javascript
  // bad
  const errorMessage = 'This is a super long error that was thrown because \
  of Batman. When you stop to think about how Batman had anything to do \
  with this, you would get nowhere \
  fast.';

  // bad
  const errorMessage = "This is a super long error that was thrown because " +
    "of Batman. When you stop to think about how Batman had anything to do " +
    "with this, you would get nowhere fast.";

  // good
  const errorMessage = "This is a super long error that was thrown because of Batman. When you stop to think about how Batman had anything to do with this, you would get nowhere fast.";
```

  
 
### 6.3 Template strings
  When programmatically building up strings, use template strings instead of concatenation. 

  > Why? Template strings give you a readable, concise syntax with proper newlines and string interpolation features.

```javascript
  // bad
  function sayHi(name) {
    return "How are you, ' + name + '?";
  }

  // bad
  function sayHi(name) {
    return ["How are you, ', name, '?"].join();
  }

  // good
  function sayHi(name) {
    return `How are you, ${name}?`;
  }
```

  
 
### 6.4 Never use eval
  Never use `eval()` on a string, it opens too many vulnerabilities. 

  
 
### 6.5 Never unnecessarily escape characters
  Do not unnecessarily escape characters in strings. 

  > Why? Backslashes harm readability, thus they should only be present when necessary.

```javascript
  // bad
  const foo = "\'this\' \i\s \"quoted\"";

  // good
  const foo = "\'this\' is "quoted"";
  const foo = `my name is "${name}"`;
```



----
## Functions

  
 
### 7.1 IIFE
  Wrap immediately invoked function expressions in parentheses. 

  > Why? An immediately invoked function expression is a single unit - wrapping both it, and its invocation parens, in parens, cleanly expresses this. Note that in a world with modules everywhere, you almost never need an IIFE.

```javascript
  // immediately-invoked function expression (IIFE)
  (function() {
    console.log("Welcome to the Internet. Please follow me.");
  }());
```

  
 
### 7.2 Block declarations
  Never declare a function in a non-function block (`if`, `while`, etc). Assign the function to a variable instead. Browsers will allow you to do it, but they all interpret it differently, which is bad news. 

  **Note:** ECMA-262 defines a `block` as a list of statements. A function declaration is not a statement.

```javascript
  // bad
  if (currentUser) {
    function test() {
      console.log("Nope.");
    }
  }

  // decent
  let test = null;
  if (currentUser) {
    test = () => {
      console.log("Yup.");
    };
  }
```

  
 
### 7.4 Never override arguments parameter
  Never name a parameter `arguments`. This will take precedence over the `arguments` object that is given to every function scope.

```javascript
  // bad
  function foo(name, options, arguments) {
    // ...
  }

  // good
  function foo(name, options, args) {
    // ...
  }
```

  
 
### 7.5 Use spread over arguments
  Never use `arguments`, opt to use rest syntax `...` instead. 

  > Why? `...` is explicit about which arguments you want pulled. Plus, rest arguments are a real Array, and not merely Array-like like `arguments`.

```javascript
  // bad
  function concatenateAll() {
    const args = Array.prototype.slice.call(arguments);
    return args.join("");
  }

  // good
  function concatenateAll(...args) {
    return args.join("");
  }
```

  
 
### 7.6 Default parameter values
  Use default parameter syntax rather than mutating function arguments.

```javascript
  // really bad
  function handleThings(opts) {
    // No! We shouldn’t mutate function arguments.
    // Double bad: if opts is falsy it'll be set to an object which may
    // be what you want but it can introduce subtle bugs.
    opts = opts || {};
    // ...
  }

  // still bad
  function handleThings(opts) {
    if (opts === void 0) {
      opts = {};
    }
    // ...
  }

  // good
  function handleThings(opts = {}) {
    // ...
  }
```

  
 
### 7.7 Never cause side effects with default parameters
  Avoid side effects with default parameters.

  > Why? They are confusing to reason about.

```javascript
  var b = 1;
  // bad
  function count(a = b++) {
    console.log(a);
  }
  count();  // 1
  count();  // 2
  count(3); // 3
  count();  // 3
```

  
 
### 7.8 Default parameter last
  Always put default parameters last.

```javascript
  // bad
  function handleThings(opts = {}, name) {
    // ...
  }

  // good
  function handleThings(name, opts = {}) {
    // ...
  }
```

  
 
### 7.9 Function declaration
  Never use the Function constructor to create a new function. 

  > Why? Creating a function in this way evaluates a string similarly to `eval()`, which opens vulnerabilities.

```javascript
  // bad
  var add = new Function("a', 'b', 'return a + b");

  // still bad
  var subtract = Function("a', 'b', 'return a - b");

  //good
  function add(a, b) {
    return a + b
  }

  //good
  const add = (a, b) => (a + b);
```

 
### 7.10 Clean functions
  Aim to primarily use "clean" functions when dealing with operations which are not required to mutate object / array references. 
  
  > A "clean" function is a function which has no dependency on outside state. Given the same input, it should **always** return the same value.

```javascript
  //bad
  let b = 1;

  function add(value) {
    return value + b;
  }

  //good
  function add(value, otherValue) {
    return value + otherValue;
  }
```

 
### 7.11 Mutator functions
  Only use a "mutator" function when the function is descriptive in its purpose, working with objects or arrays and there is some consideration towards performance.

   > A "mutator" function is a function which does not rely on outside state, but can modify a reference which gets passed in.

   When dealing with small objects / arrays and performance is not a primary concern, returning a new reference would be better!

```javascript
   //bad
   var config = { 
     sourceDirectory: "./",
     outputDirectory: "",
     copy: []
   };

   function populateConfig(toCopy) {
     config.copy = toCopy;
   }

   //decent (good for more performant required operations)
   var config = { 
     sourceDirectory: "./",
     outputDirectory: "",
     copy: []
   };

   function populateConfig(config, toCopy) {
    config.copy = toCopy;
   }

   //good (for small objects/arrays)
   var config = { 
     sourceDirectory: "./",
     outputDirectory: "",
     copy: []
   };

   function populateConfig(config, toCopy) {
     return { ...config, copy: toCopy };
   }
```
  

 
### 7.12 Concise functions
  Aim to write small functions, typically less than 75 lines of code. 

   > Why? Small functions are clearer in purpose, easier to maintain, easier to test and less prone to instability / hidden bugs.  ‏‏‎ 

   ‏‏‎ 

 
### 7.13 Never reassign parameter values
  Never reassign parameters. 

  > Why? Reassigning parameters can lead to unexpected behaviour, especially when accessing the `arguments` object. It can also cause optimization issues, especially in V8.

```javascript
  // bad
  function f1(a) {
    a = 1;
    // ...
  }

  function f2(a) {
    if (!a) { a = 1; }
    // ...
  }

  // good
  function f3(a) {
    const b = a || 1;
    // ...
  }

  function f4(a = 1) {
    // ...
  }
```

  
 
### 7.14 Calling variadic functions
  Prefer the use of the spread operator `...` to call variadic functions. 

  > Why? It’s cleaner, you don’t need to supply a context, and you can not easily compose `new` with `apply`.

```javascript
  // bad
  const x = [1, 2, 3, 4, 5];
  console.log.apply(console, x);

  // good
  const x = [1, 2, 3, 4, 5];
  console.log(...x);

  // bad
  new (Function.prototype.bind.apply(Date, [null, 2016, 8, 5]));

  // good
  new Date(...[2016, 8, 5]);
```



### 7.15 Multiple return values
  Use objects for multiple return values, not arrays.

  > Why? You can add new properties over time or change the order of things without breaking call sites.

```javascript
  // bad
  function processInput(input) {
    // then a miracle occurs
    return [left, right, top, bottom];
  }

  // the caller needs to think about the order of return data
  const [left, __, top] = processInput(input);

  // good
  function processInput(input) {
    // then a miracle occurs
    return { 
      left: leftValue, 
      right: rightValue, 
      top: topValue, 
      bottom: bottomValue
    };
  }

  // the caller selects only the data they need
  const { left, top } = processInput(input);
```

### 7.16 Context
  Don’t save references to `this`. Use arrow functions or [Function#bind](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind).

```javascript
  // bad
  function foo() {
    const self = this;
    return function() {
    console.log(self);
    };
  }

  // bad
  function foo() {
    const that = this;
    return function() {
    console.log(that);
    };
  }

  // good
  function foo() {
    return () => {
    console.log(this);
    };
  }
```


----
## Arrow Functions

  
 
### 8.1 Anonymous functions should be arrow function syntax
  When you must use an anonymous function (as when passing an inline callback), use arrow function notation. 

  > Why? It creates a version of the function that executes in the context of `this`, which is usually what you want, and is a more concise syntax.

  > Why not? If you have a fairly complicated function, you might move that logic out into its own named function expression.

```javascript
  // bad
  [1, 2, 3].map(function (x) {
    const y = x + 1;
    return x * y;
  });

  // good
  [1, 2, 3].map((x) => {
    const y = x + 1;
    return x * y;
  });
```

  
 
### 8.2 Implicit returns
  If the function body consists of a single statement returning an [expression](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Expressions_and_Operators#Expressions) without side effects, omit the braces and use the implicit return. Otherwise, keep the braces and use a `return` statement. 

  > Why? Syntactic sugar. It reads well when multiple functions are chained together.

```javascript
  // bad
  [1, 2, 3].map(number => {
    const nextNumber = number + 1;
    `A string containing the ${nextNumber}.`;
  });

  // good
  [1, 2, 3].map(number => `A string containing the ${number + 1}.`);

  // good
  [1, 2, 3].map((number) => {
    const nextNumber = number + 1;
    return `A string containing the ${nextNumber}.`;
  });

  // No implicit return with side effects
  function foo(callback) {
    const val = callback();
    if (val === true) {
    // Do something if callback returns true
    }
  }

  let bool = false;

  // bad
  foo(() => bool = true);

  // good
  foo(() => {
    bool = true;
  });
```

  
 
### 8.3 Multi-line implicit returns
  In case the expression spans over multiple lines, wrap it in parentheses for better readability.

  > Why? It shows clearly where the function starts and ends.

```javascript
  // bad
  ["get', 'post', 'put"].map(httpMethod => Object.prototype.hasOwnProperty.call(
    httpMagicObjectWithAVeryLongName,
    httpMethod,
    )
  );

  // good
  ["get', 'post', 'put"].map(httpMethod => (
    Object.prototype.hasOwnProperty.call(
    httpMagicObjectWithAVeryLongName,
    httpMethod,
    )
  ));
```

  
 
### 8.4 Implicit returns with comparison operators
  Avoid confusing arrow function syntax (`=>`) with comparison operators (`<=`, `>=`). 

```javascript
  // bad
  const itemHeight = item => item.height <= 256 ? item.largeSize : item.smallSize;

  // bad
  const itemHeight = (item) => item.height >= 256 ? item.largeSize : item.smallSize;

  // good
  const itemHeight = item => (item.height <= 256 ? item.largeSize : item.smallSize);

  // good
  const itemHeight = (item) => {
    const { height, largeSize, smallSize } = item;
    return height <= 256 ? largeSize : smallSize;
  };
```

  
 
### 8.5  Implicit return line-breaks
  Enforce the location of arrow function bodies with implicit returns. 

```javascript
  // bad
  foo =>
    bar;

  foo =>
    (bar);

  // good
  foo => bar;
  foo => (bar);
```



----
## Classes and Constructors

  
 
### 9.1 Avoid prototype
  Always use `class`. Avoid manipulating `prototype` directly.

  > Why? `class` syntax is more concise and easier to reason about.

```javascript
  // bad
  function Queue(contents = []) {
    this.queue = [...contents];
  }
  Queue.prototype.pop = function() {
    const value = this.queue[0];
    this.queue.splice(0, 1);
    return value;
  };

  // good
  class Queue {
    constructor(contents = []) {
      this.queue = [...contents];
    }
    pop() {
      const value = this.queue[0];
      this.queue.splice(0, 1);
      return value;
    }
  }
```

  
 
### 9.2 Inheritance
  Use `extends` for inheritance.

  > Why? It is a built-in way to inherit prototype functionality without breaking `instanceof`.

  *Note: Remember composition should be used/tried before immediately reaching for Class hierarchies!*

```javascript
  // bad
  const inherits = require("inherits");
  function PeekableQueue(contents) {
    Queue.apply(this, contents);
  }
  inherits(PeekableQueue, Queue);
  PeekableQueue.prototype.peek = function() {
    return this.queue[0];
  };

  // good
  class PeekableQueue extends Queue {
    peek() {
      return this.queue[0];
    }
  }
```

  
 
### 9.3 Method chaining
  Methods can return `this` to help with method chaining.

```javascript
  // bad
  Jedi.prototype.jump = function() {
    this.jumping = true;
    return true;
  };

  Jedi.prototype.setHeight = function (height) {
    this.height = height;
  };

  const luke = new Jedi();
  luke.jump(); // => true
  luke.setHeight(20); // => undefined

  // good
  class Jedi {
    jump() {
      this.jumping = true;
      return this;
    }

    setHeight(height) {
      this.height = height;
      return this;
    }
  }

  const luke = new Jedi();

  luke.jump()
    .setHeight(20);
```

  
 
### 9.4 ToString
  It’s okay to write a custom `toString()` method, just make sure it works successfully and causes no side effects.

```javascript
  class Jedi {
    constructor(options = {}) {
      this.name = options.name || "no name";
    }

    getName() {
      return this.name;
    }

    toString() {
      return `Jedi - ${this.getName()}`;
    }
  }
```

  
 
### 9.5 Empty constructors
  Classes have a default constructor if one is not specified. An empty constructor function or one that just delegates to a parent class is unnecessary. 

```javascript
  // bad
  class Jedi {
    constructor() {}

    getName() {
      return this.name;
    }
  }

  // bad
  class Rey extends Jedi {
    constructor(...args) {
    super(...args);
    }
  }

  // good
  class Rey extends Jedi {
    constructor(...args) {
    super(...args);
      this.name = "Rey";
    }
  }
```

  
 
### 9.6 Duplicate class members
  Avoid duplicate class members. 

  > Why? Duplicate class member declarations will silently prefer the last one - having duplicates is almost certainly a bug.

```javascript
  // bad
  class Foo {
    bar() { return 1; }
    bar() { return 2; }
  }

  // good
  class Foo {
    bar() { return 1; }
  }

  // good
  class Foo {
    bar() { return 2; }
  }
```



----
## Modules

  
 
### 10.1 Import / export
  Always use modules (`import`/`export`) over a non-standard module system. You can always transpile to your preferred module system.

  > Why? Modules are the future, let’s start using the future now.

```javascript
  // bad
  const PebbleStyleGuide = require("./PebbleStyleGuide");
  module.exports = PebbleStyleGuide.es6;

  // ok
  import PebbleStyleGuide from "./PebbleStyleGuide";
  export default PebbleStyleGuide.es6;

  // best
  import { es6 } from "./PebbleStyleGuide";
  export es6;
```

  
 
### 10.2 Wildcard imports
  Do not use wildcard imports.

  > Why? This makes sure you have a single default export.

```javascript
  // bad
  import * as PebbleStyleGuide from "./PebbleStyleGuide";

  // good
  import PebbleStyleGuide from "./PebbleStyleGuide";
```
  
 
### 10.3 Importing multiple items
  Only import from a path in one place.
 
  > Why? Having multiple lines that import from the same path can make code harder to maintain.

```javascript
  // bad
  import foo from "foo";
  // … some other imports … //
  import { named1, named2 } from "foo";

  // good
  import foo, { named1, named2 } from "foo";

  // good
  import foo, {
    named1,
    named2,
  } from "foo";
```

  
 
### 10.4 Never export mutable variables
  Do not export mutable bindings.
 
  > Why? Mutation should be avoided in general, but in particular when exporting mutable bindings. While this technique may be needed for some special cases, in general, only constant references should be exported.

```javascript
  // bad
  export let foo = 3;

  // good
  export const foo = 3;
```

  
 
### 10.5 Named exports
  Always use a named export, regardless of if a file only contains one exported item.

```javascript
  // bad
  export default function foo() {}

  // good
  export function foo() {}
```

  
 
### 10.6 Grouped imports
  Put all `import`s above non-import statements.
 
  > Why? Since `import`s are hoisted, keeping them all at the top prevents surprising behaviour.

```javascript
  // bad
  import foo from "foo";
  foo.init();

  import bar from "bar";

  // good
  import foo from "foo";
  import bar from "bar";

  foo.init();
```

  
 
### 10.7 Multi-line imports
  Multi-line imports should be indented just like multi-line array and object literals.

  > Why? The curly braces follow the same indentation rules as every other curly brace block in the style guide, as do the trailing commas.

```javascript
  // bad
  import {longNameA, longNameB, longNameC, longNameD, longNameE} from "path";

  // good
  import {
    longNameA,
    longNameB,
    longNameC,
    longNameD,
    longNameE,
  } from "path";
```

  
 
### 10.8 No custom import syntax
  Disallow Webpack loader syntax in module import statements.
 
  > Why? Since using Webpack syntax in the imports couples the code to a module bundler. Prefer using the loader syntax in `webpack.config.js`.

```javascript
  // bad
  import fooSass from "css!sass!foo.scss";
  import barCss from "style!css!bar.css";

  // good
  import fooSass from "foo.scss";
  import barCss from "bar.css";
```



----
## Iterators and Generators

  
 
### 11.1 Iterators
  Try to use high-order functions over iterators when expecting a single result.

  > Use `map()` / `every()` / `filter()` / `find()` / `findIndex()` / `reduce()` / `some()` / ... to iterate over arrays, and `Object.keys()` / `Object.values()` / `Object.entries()` to produce arrays so you can iterate over objects.
  
  > Note: on very large data sets a traditional `for` loop maybe worth considering when performance is paramount.

```javascript
  const numbers = [1, 2, 3, 4, 5];

  //bad
  let sum = 0;
  for (var i = 0; i < numbers.length; i++) {
    sum += num[i];
  }
  sum === 15;

  //bad (sum mutation within arrow function) 
  let sum = 0;
  numbers.forEach((num) => sum += num);

  // decent
  let sum = 0;
  for (let num of numbers) {
    sum += num;
  }
  sum === 15;

  // best (use the functional force)
  const sum = numbers.reduce((total, num) => total + num, 0);
  sum === 15;

  // bad
  const increasedByOne = [];
  for (let i = 0; i < numbers.length; i++) {
    increasedByOne.push(numbers[i] + 1);
  }

  // best (keeping it functional)
  const increasedByOne = numbers.map(num => num + 1);
```


### 11.2 Generators 

  Don’t use generators for now.

  > Why? They don’t transpile well to ES5.

  If you **must** use generators, make sure their function signature is spaced properly. 

  > Why? `function` and `*` are part of the same conceptual keyword - `*` is not a modifier for `function`, `function*` is a unique construct, different from `function`.

```javascript
  // bad
  function * foo() {
    // ...
  }

  // bad
  const bar = function * () {
    // ...
  };

  // bad
  const baz = function *() {
    // ...
  };

  // bad
  const quux = function*() {
    // ...
  };

  // bad
  function*foo() {
    // ...
  }

  // bad
  function *foo() {
    // ...
  }

  // very bad
  function
  *
  foo() {
    // ...
  }

  // very bad
  const wat = function
  *
  () {
    // ...
  };

  // good
  function* foo() {
    // ...
  }

  // good
  const foo = function* () {
    // ...
  };
```



----
## Properties

  
 
### 12.1 Accessing properties
  Use dot notation when accessing properties. 

```javascript
  const luke = {
    jedi: true,
    age: 28
  };

  // bad
  const isJedi = luke["jedi"];

  // good
  const isJedi = luke.jedi;
```

  
 
### 12.2 Accessing properties via variable
  Use bracket notation `[]` when accessing properties with a variable.

```javascript
  const luke = {
    jedi: true,
    age: 28
  };

  function getProp(prop) {
    return luke[prop];
  }

  const isJedi = getProp("jedi");
```



----
## Variables

  
 
### 13.1 Declaration
  Always use `const` or `let` to declare variables. Not doing so will result in global variables. We want to avoid polluting the global namespace. Captain Planet warned us of that. 

```javascript
  // bad
  superPower = new SuperPower();

  // good
  const superPower = new SuperPower();
```

  
 
### 13.2 One declaration, one line, one assignment
  Use one `const` or `let` declaration per variable or assignment. 

  > Why? It’s easier to add new variable declarations this way, and you never have to worry about swapping out a `;` for a `,` or introducing punctuation-only diffs. You can also step through each declaration with the debugger, instead of jumping through all of them at once.

```javascript
  // bad
  const items = getItems(),
    goSportsTeam = true,
    dragonball = "z";

  // bad
  // (compare to above, and try to spot the mistake)
  const items = getItems(),
    goSportsTeam = true;
    dragonball = "z";

  // good
  const items = getItems();
  const goSportsTeam = true;
  const dragonball = "z";
```

  
 
### 13.3 Grouped declarations
  Group all your `const`s and then group all your `let`s.

  > Why? This is helpful when later on you might need to assign a variable depending on one of the previous assigned variables.

```javascript
  // bad
  let i = 0, len = 10, dragonball = null,
    items = getItems(),
    goSportsTeam = true;

  // bad
  let i = 0;
  const items = getItems();
  let dragonball = null;
  const goSportsTeam = true;
  let len = 10;

  // good
  const goSportsTeam = true;
  const items = getItems();
  let dragonball = null;
  let i = 0;
  let length = 10;
```

  
 
### 13.4 Declare only when needed
  Assign variables where you need them, but place them in a reasonable place.

  > Why? `let` and `const` are block scoped and not function scoped.

```javascript
  // bad - unnecessary function call
  function checkName(hasName) {
    const name = getName();

    if (hasName === "test") {
      return false;
    }

    if (name === "test") {
      this.setName("");
      return false;
    }

    return name;
  }

  // good
  function checkName(hasName) {
    if (hasName === "test") {
      return false;
    }

    const name = getName();

    if (name === "test") {
      this.setName("");
      return false;
    }

    return name;
  }
```
  
 
### 13.5 Never chain variable assignments
  Don’t chain variable assignments. 

  > Why? Chaining variable assignments creates implicit global variables.

```javascript
  // bad
  (function example() {
    // JavaScript interprets this as
    // let a = ( b = ( c = 1 ) );
    // The let keyword only applies to variable a; variables b and c become
    // global variables.
    let a = b = c = 1;
  }());

  console.log(a); // throws ReferenceError
  console.log(b); // 1
  console.log(c); // 1

  // good
  (function example() {
    let a = 1;
    let b = a;
    let c = a;
  }());

  console.log(a); // throws ReferenceError
  console.log(b); // throws ReferenceError
  console.log(c); // throws ReferenceError

  // the same applies for `const`
```


 
### 13.6 Line breaked declarations
  Avoid linebreaks before or after `=` in an assignment. If your assignment violates [`max-len`](https://eslint.org/docs/rules/max-len.html), surround the value in parens. eslint [`operator-linebreak`](https://eslint.org/docs/rules/operator-linebreak.html).

  > Why? Linebreaks surrounding `=` can obfuscate the value of an assignment.

```javascript
  // bad
  const foo =
    superLongLongLongLongLongLongLongLongFunctionName();

  // bad
  const foo
    = "superLongLongLongLongLongLongLongLongString";

  // good
  const foo = (
    superLongLongLongLongLongLongLongLongFunctionName()
  );

  // good
  const foo = "superLongLongLongLongLongLongLongLongString";

  //decent (only when chaining)
  const foo = stream.pipe(otherStream)
      .pipe(anotherStream)
      .pipe(anotherOtherStream);
```


 
### 13.7 Unused variables
  Disallow unused variables. 

  > Why? Variables that are declared and not used anywhere in the code are most likely an error due to incomplete refactoring. Such variables take up space in the code and can lead to confusion by readers.

```javascript
  // bad

  var some_unused_var = 42;

  // Write-only variables are not considered as used.
  var y = 10;
  y = 5;

  // A read for a modification of itself is not considered as used.
  var z = 0;
  z = z + 1;

  // Unused function arguments.
  function getX(x, y) {
    return x;
  }

  // good

  function getXPlusY(x, y) {
    return x + y;
  }

  var x = 1;
  var y = a + 2;

  alert(getXPlusY(x, y));

  // "type" is ignored even if unused because it has a rest property sibling.
  // This is a form of extracting an object that omits the specified keys.
  var { type, ...coords } = data;
  // "coords' is now the 'data' object without its 'type" property.
```



----
## Hoisting

  
 
### 14.1 var declarations
  `var` declarations get hoisted to the top of their closest enclosing function scope, their assignment does not. `const` and `let` declarations are blessed with a new concept called [Temporal Dead Zones (TDZ)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/let#Temporal_Dead_Zone). It’s important to know why [typeof is no longer safe](http://es-discourse.com/t/why-typeof-is-no-longer-safe/15).

```javascript
  // we know this wouldn’t work (assuming there
  // is no notDefined global variable)
  function example() {
    console.log(notDefined); // => throws a ReferenceError
  }

  // creating a variable declaration after you
  // reference the variable will work due to
  // variable hoisting. Note: the assignment
  // value of `true` is not hoisted.
  function example() {
    console.log(declaredButNotAssigned); // => undefined
    var declaredButNotAssigned = true;
  }

  // the interpreter is hoisting the variable
  // declaration to the top of the scope,
  // which means our example could be rewritten as:
  function example() {
    let declaredButNotAssigned;
    console.log(declaredButNotAssigned); // => undefined
    declaredButNotAssigned = true;
  }

  // using const and let
  function example() {
    console.log(declaredButNotAssigned); // => throws a ReferenceError
    console.log(typeof declaredButNotAssigned); // => throws a ReferenceError
    const declaredButNotAssigned = true;
  }
```

  
 
### 14.2 Anonymous function expressions
  Anonymous function expressions hoist their variable name, but not the function assignment.

```javascript
  function example() {
    console.log(anonymous); // => undefined

    anonymous(); // => TypeError anonymous is not a function

    var anonymous = function() {
      console.log("anonymous function expression");
    };
  }
```

  
 
### 14.3 Named function expressions
  Named function expressions hoist the variable name, not the function name or the function body.

```javascript
  function example() {
    console.log(named); // => undefined

    named(); // => TypeError named is not a function

    superPower(); // => ReferenceError superPower is not defined

    var named = function superPower() {
      console.log("Flying");
    };
  }

  // the same is true when the function name
  // is the same as the variable name.
  function example() {
    console.log(named); // => undefined

    named(); // => TypeError named is not a function

    var named = function named() {
      console.log("named");
    };
  }
```

  
 
### 14.4 Named functions are always hoisted
  Function declarations hoist their name and the function body.

```javascript
  function example() {
    superPower(); // => Flying

    function superPower() {
      console.log("Flying");
    }
  }
```

  - For more information refer to [JavaScript Scoping and Hoisting](http://www.adequatelygood.com/2010/2/JavaScript-Scoping-and-Hoisting/) by [Ben Cherry](http://www.adequatelygood.com/).



----
## Comparison Operators and Equality

  
 
### 15.1 Strict equality
  Use `===` and `!==` over `==` and `!=`. 

  > Why? Loose equality checks can lead to unintended bugs - https://dorey.github.io/JavaScript-Equality-Table/

  
 
### 15.2 Conditional statement order
  Conditional statements such as the `if` statement evaluate their expression using coercion with the `ToBoolean` abstract method and always follow these simple rules:

  - **Objects** evaluate to **true**
  - **Undefined** evaluates to **false**
  - **Null** evaluates to **false**
  - **Booleans** evaluate to **the value of the boolean**
  - **Numbers** evaluate to **false** if **+0, -0, or NaN**, otherwise **true**
  - **Strings** evaluate to **false** if an empty string `""`, otherwise **true**

```javascript
  if ([0] && []) {
    // true
    // an array (even an empty one) is an object, objects will evaluate to true
  }
```

  
 
### 15.3 Conditional statement checks
  Use shortcuts for booleans, but explicit comparisons for strings and numbers.

```javascript
  // bad
  if (isValid === true) {
    // ...
  }

  // good
  if (isValid) {
    // ...
  }

  // bad
  if (name) {
    // ...
  }

  // good
  if (name !== "") {
    // ...
  }

  // bad
  if (collection.length) {
    // ...
  }

  // good
  if (collection.length > 0) {
    // ...
  }
```

 
### 15.4 Ternaries
  Ternaries should not be nested and generally be single line expressions. 

```javascript
  // bad
  const foo = maybe1 > maybe2
    ? "bar"
    : value1 > value2 ? "baz" : null;

  // split into 2 separated ternary expressions
  const maybeNull = value1 > value2 ? "baz" : null;

  // better
  const foo = maybe1 > maybe2
    ? "bar"
    : maybeNull;

  // best
  const foo = maybe1 > maybe2 ? "bar" : maybeNull;
```

  
 
### 15.5 Unnecessary ternaries
  Avoid unneeded ternary statements. 

```javascript
  // bad
  const foo = a ? a : b;
  const bar = c ? true : false;
  const baz = c ? false : true;

  // good
  const foo = a || b;
  const bar = Boolean(c);
  const bar = !!c;
  const baz = !c;
```

  
 
### 15.6 Mixing operators
  When mixing operators, enclose them in parentheses. The only exception is the standard arithmetic operators (`+`, `-`, `*`, and `/`) since their precedence is broadly understood. 

  > Why? This improves readability and clarifies the developer’s intention.

```javascript
  // bad
  const foo = a && b < 0 || c > 0 || d + 1 === 0;

  // bad
  const bar = a ** b - 5 % d;

  // bad
  // one may be confused into thinking (a || b) && c
  if (a || b && c) {
    return d;
  }

  // good
  const foo = (a && b < 0) || c > 0 || (d + 1 === 0);

  // good
  const bar = (a ** b) - (5 % d);

  // good
  if (a || (b && c)) {
    return d;
  }

  // good
  const bar = a + b / c * d;
```



----
## Blocks
 
### 16.1 Braced blocks
  Always uses braces and multi-line blocks. 

```javascript
  // bad
  if (test)
    return false;

  // bad
  if (test) return false;

  // good
  if (test) {
    return false;
  }

  // bad
  function foo() { return false; }

  // good
  function bar() {
    return false;
  }
```

  
 
### 16.2  Multi-line conditional blocks
  If you’re using multi-line blocks with `if` and `else`, put `else` on the same line as your `if` block’s closing brace. 

```javascript
  // bad
  if (test) {
    thing1();
    thing2();
  }
  else {
    thing3();
  }

  // good
  if (test) {
    thing1();
    thing2();
  } else {
    thing3();
  }
```

  
 
### 16.3 if block return statements
  If an `if` block always executes a `return` statement, the subsequent `else` block is unnecessary. A `return` in an `else if` block following an `if` block that contains a `return` can be separated into multiple `if` blocks. 

```javascript
  // bad
  function foo() {
    if (x) {
      return x;
    } else {
      return y;
    }
  }

  // bad
  function cats() {
    if (x) {
      return x;
    } else if (y) {
      return y;
    }
  }

  // bad
  function dogs() {
    if (x) {
      return x;
    } else {
      if (y) {
        return y;
      }
    }
  }

  // good
  function foo() {
    if (x) {
      return x;
    }

    return y;
  }

  // good
  function cats() {
    if (x) {
      return x;
    }

    if (y) {
      return y;
    }
  }

  // good
  function dogs(x) {
    if (x) {
      if (z) {
        return y;
      }
    } else {
      return z;
    }
  }
```

### 16.4 Switch cases
  Use braces to create blocks in `case` and `default` clauses that contain lexical declarations (e.g. `let`, `const`, `function`, and `class`). 

  > Why? Lexical declarations are visible in the entire `switch` block but only get initialized when assigned, which only happens when its `case` is reached. This causes problems when multiple `case` clauses attempt to define the same thing.

```javascript
  // bad
  switch (foo) {
    case 1:
      let x = 1;
      break;
    case 2:
      const y = 2;
      break;
    case 3:
      function f() {
        // ...
      }
      break;
    default:
      class C {}
  }

  // good
  switch (foo) {
    case 1: {
      let x = 1;
      break;
    }
    case 2: {
      const y = 2;
      break;
    }
    case 3: {
      function f() {
        // ...
      }
      break;
    }
    case 4:
      bar();
      break;
    default: {
      class C {}
    }
  }
```


----
## Control Statements

  
 
### 17.1 Long / complex statements
  In case your control statement (`if`, `while` etc.) gets too long or exceeds the maximum line length, each (grouped) condition could be put into a new line.

```javascript
  // bad
  if ((foo === 123 || bar === "abc") && doesItLookGoodWhenItBecomesThatLong() && isThisReallyHappening()) {
    thing1();
  }

  // bad
  if (foo === 123 &&
    bar === "abc") {
    thing1();
  }

  // bad
  if (
    foo === 123 &&
    bar === "abc"
  ) {
    thing1();
  }

  // good
  if (
    (foo === 123 || bar === "abc") &&
    doesItLookGoodWhenItBecomesThatLong() &&
    isThisReallyHappening()
  ) {
    thing1();
  }

  // good
  if (foo === 123 && bar === "abc") {
    thing1();
  }
```

  
 
### 17.2 Always used control statements over non-assigned operators
  Don't use selection operators in place of control statements.

```javascript
  // bad
  !isRunning && startRunning();

  // good
  if (!isRunning) {
    startRunning();
  }
```



----
## Comments

  
 
### 18.1 Future changes comments
  Prefixing your comments with `FIXME` or `TODO` helps other developers quickly understand if you’re pointing out a problem that needs to be revisited, or if you’re suggesting a solution to the problem that needs to be implemented. These are different than regular comments because they are actionable. The actions are `FIXME: -- need to figure this out` or `TODO: -- need to implement`.

  
 
### 18.2 FIXME
  Use `// FIXME:` to annotate problems.

```javascript
  class Calculator extends Abacus {
    constructor() {
    super();
      // FIXME: shouldn’t use a global here
      total = 0;
    }
  }
```

  
 
### 18.3 TODO
  Use `// TODO:` to annotate solutions to problems.

```javascript
  class Calculator extends Abacus {
    constructor() {
    super();
      // TODO: total should be configurable by an options param
      this.total = 0;
    }
  }
```

### 18.4 When to add comments
  Only add comments when something is:

   - Complex in nature.
   - Dealing with a language / browser bug or quirk.
   - Adding a **useful** `// TODO:` or `// FIXME:`.
   - JsDoc typings.

   > Why? Two reasons.
   The first is that code readability **should be a priority**. If the code is readable, then the need for comments is nearly null void (no point in stating the obvious!).
   The second is that comments can frequently be left out of date or forgotten about during code refactors. If this occurs, it can deliver misinformation to a developer.

   If a comment explains something which is obvious or has no real meaning / value, then it does not belong!



----
## Commas

  
 
### 19.1 Leading commas
  Leading commas: **Nope.** 

```javascript
  // bad
  const story = [
    once
    , upon
    , aTime
  ];

  // good
  const story = [
    once,
    upon,
    aTime
  ];

  // bad
  const hero = {
    firstName: "Ada"
    , lastName: "Lovelace"
    , birthYear: 1815
    , superPower: "computers"
  };

  // good
  const hero = {
    firstName: "Ada",
    lastName: "Lovelace",
    birthYear: 1815,
    superPower: "computers"
  };
```

----

## Semicolons

  
 
### 20.1 Always use semi-colons
  **Yup.** 

  > Why? When JavaScript encounters a line break without a semicolon, it uses a set of rules called [Automatic Semicolon Insertion](https://tc39.github.io/ecma262/#sec-automatic-semicolon-insertion) to determine whether or not it should regard that line break as the end of a statement, and (as the name implies) place a semicolon into your code before the line break if it thinks so. ASI contains a few eccentric behaviours, though, and your code will break if JavaScript misinterprets your line break. These rules will become more complicated as new features become a part of JavaScript. Explicitly terminating your statements and configuring your linter to catch missing semicolons will help prevent you from encountering issues.

```javascript
  // bad - raises exception
  const luke = {}
  const leia = {}
  [luke, leia].forEach(jedi => jedi.father = "vader")

  // bad - raises exception
  const reaction = "No! That’s impossible!"
  (async function meanwhileOnTheFalcon() {
    // handle `leia`, `lando`, `chewie`, `r2`, `c3p0`
    // ...
  }())

  // bad - returns `undefined` instead of the value on the next line - always happens when `return` is on a line by itself because of ASI!
  function foo() {
    return
    "search your feelings, you know it to be foo"
  }

  // good
  const luke = {};
  const leia = {};
  [luke, leia].forEach((jedi) => {
    jedi.father = "vader";
  });

  // good
  const reaction = "No! That’s impossible!";
  (async function meanwhileOnTheFalcon() {
    // handle `leia`, `lando`, `chewie`, `r2`, `c3p0`
    // ...
  }());

  // good
  function foo() {
    return "search your feelings, you know it to be foo";
  }
```

  [Read more](https://stackoverflow.com/questions/7365172/semicolon-before-self-invoking-function/7365214#7365214).



----
## Type Casting and Coercion

  
 
### 21.1 Start off with coercion
  Perform type coercion at the beginning of the statement.

  
 
### 21.2 Strings
   Strings: 

```javascript
  // => this.reviewScore = 9;

  // bad
  const totalScore = new String(this.reviewScore); // typeof totalScore is "object" not "string"

  // bad
  const totalScore = this.reviewScore + ""; // invokes this.reviewScore.valueOf()

  // decent
  const totalScore = this.reviewScore.toString(); // isn’t guaranteed to return a string

  // good
  const totalScore = String(this.reviewScore);
```

  
 
### 21.3 Numbers
  Numbers: Use `Number` for type casting and `parseInt` always with a radix for parsing strings. 

```javascript
  const inputValue = "4";

  // bad
  const val = new Number(inputValue);

  // bad
  const val = +inputValue;

  // bad
  const val = inputValue >> 0;

  // bad
  const val = parseInt(inputValue);

  // good
  const val = Number(inputValue);

  // good
  const val = parseInt(inputValue, 10);
```

  
 
### 21.4 Performant number parsing
  If for whatever reason you are doing something wild and `parseInt` is your bottleneck and need to use Bitwise for [performance reasons](https://jsperf.com/coercion-vs-casting/3), leave a comment explaining why and what you are doing.

```javascript
  // good
  /**
   * parseInt was the reason my code was slow.
   * Bitwiseing the String to coerce it to a
   * Number made it a lot faster.
   */
  const val = inputValue >> 0;
```

  
 
### 21.5 Bitwise
  **Note:** Be careful when using bitwise operations. Numbers are represented as [64-bit values](https://es5.github.io/#x4.3.19), but bitwise operations always return a 32-bit integer. Bitwise can lead to unexpected behaviour for integer values larger than 32 bits.

```javascript
  2147483647 >> 0; // => 2147483647
  2147483648 >> 0; // => -2147483648
  2147483649 >> 0; // => -2147483647
```

  
 
### 21.6 Booleans
  Booleans: 

```javascript
  const age = 0;

  // bad
  const hasAge = new Boolean(age);

  // good
  const hasAge = Boolean(age);

  // good
  const hasAge = !!age;
```



----
## Naming Conventions

  
 
### 22.1 Concise, but descriptive name
  Avoid single letter names. Be descriptive with your naming. 

```javascript
  // bad
  function q() {
    // ...
  }

  // good
  function query() {
    // ...
  }
```

  
 
### 22.2 Camel casing
  Use camelCase when naming objects, functions, and instances. 

```javascript
  // bad
  const OBJEcttsssss = {};
  const this_is_my_object = {};
  function c() {}

  // good
  const thisIsMyObject = {};
  function thisIsMyFunction() {}
```

  
 
### 22.3 Pascal Casing
  Use PascalCase only when naming constructors or classes. 

```javascript
  // bad
  function user(options) {
    this.name = options.name;
  }

  const bad = new user({
    name: "nope",
  });

  // good
  class User {
    constructor(options) {
      this.name = options.name;
    }
  }

  const good = new User({
    name: "yup",
  });
```

  
 
### 22.4 Private properties / methods
  Use a leading underscore to denote a "private" property or method on an object/class.

```javascript
  // bad
  this.privateProp_ = "Panda";
  this.$privateProp = "Panda";

  //good
  this._private = "Panda";
```

 
### 22.5 Filenames
  A base filename should exactly match the name of its default export.

```javascript
  // file 1 contents
  class CheckBox {
    // ...
  }
  export default CheckBox;

  // file 2 contents
  export default function fortyTwo() { return 42; }

  // file 3 contents
  export default function insideDirectory() {}

  // in some other file
  // bad
  import CheckBox from "./checkBox"; // PascalCase import/export, camelCase filename
  import FortyTwo from "./FortyTwo"; // PascalCase import/filename, camelCase export
  import InsideDirectory from "./InsideDirectory"; // PascalCase import/filename, camelCase export

  // bad
  import CheckBox from "./check_box"; // PascalCase import/export, snake_case filename
  import forty_two from "./forty_two"; // snake_case import/filename, camelCase export
  import inside_directory from "./inside_directory"; // snake_case import, camelCase export
  import index from "./inside_directory/index"; // requiring the index file explicitly
  import insideDirectory from "./insideDirectory/index"; // requiring the index file explicitly

  // good
  import CheckBox from "./CheckBox"; // PascalCase export/import/filename
  import fortyTwo from "./fortyTwo"; // camelCase export/import/filename
  import insideDirectory from "./insideDirectory"; // camelCase export/import/directory name/implicit "index"
  // ^ supports both insideDirectory.js and insideDirectory/index.js
```

  
 
### 22.6 Exported classes
  Use PascalCase when you export a constructor / class / singleton / function library / bare object.

```javascript
  //Singleton
  export const StyleGuide = {
    es6: {
    },
  };

  //Class
  export class StyleGuide { }
```

  
 
### 22.8  Acronyms and initialisms
  Acronyms and initialisms should **always** follow normal naming conventions (camel / Pascal)

```javascript
  // bad
  import SMSContainer from "./containers/SMSContainer";

  // bad
  const HTTPRequests = [
    // ...
  ];

  // good
  import SmsContainer from "./containers/SmsContainer";

  // also good
  const httpRequests = [
    // ...
  ];

  // best
  import TextMessageContainer from "./containers/TextMessageContainer";

  // best
  const requests = [
    // ...
  ];
```

  
 
### 22.9 Constants
  You may optionally uppercase a constant only if it (1) is exported, (2) is a `const` (it can not be reassigned), and (3) the programmer can trust it (and its nested properties) to never change.

  > Why? This is an additional tool to assist in situations where the programmer would be unsure if a variable might ever change. UPPERCASE_VARIABLES are letting the programmer know that they can trust the variable (and its properties) not to change.
  - What about all `const` variables? - This is unnecessary, so uppercasing should not be used for constants within a file. It should be used for exported constants however.
  - What about exported objects? - Uppercase at the top level of export  (e.g. `EXPORTED_OBJECT.key`) and maintain that all nested properties do not change.

```javascript
  // bad
  const PRIVATE_VARIABLE = "should not be unnecessarily uppercased within a file";

  // bad
  export const THING_TO_BE_CHANGED = "should obviously not be uppercased";

  // bad
  export let REASSIGNABLE_VARIABLE = "do not use let with uppercase variables";

  // ---

  // allowed but does not supply semantic value
  export const apiKey = "SOMEKEY";

  // better in most cases
  export const API_KEY = "SOMEKEY";

  // ---

  // bad - unnecessarily uppercases key while adding no semantic value
  export const MAPPING = {
    KEY: "value"
  };

  // good
  export const MAPPING = {
    key: "value"
  };
```



----
## Accessors

  
 
### 23.1 Functions
  Accessor functions for properties are not required.

  
 
### 23.2 Class getters / setters
  Do not use JavaScript getters/setters as they cause unexpected side effects and are harder to test, maintain, and reason about. Instead, if you do make accessor functions, use `getVal()` and `setVal("hello")`.

```javascript
  // bad
  class Dragon {
    get age() {
    // ...
    }

    set age(value) {
    // ...
    }
  }

  // good
  class Dragon {
    getAge() {
    // ...
    }

    setAge(value) {
    // ...
    }
  }
```

  
 
### 23.3 Boolean properties / methods
  If the property/method is a `boolean`, use `isVal()` or `hasVal()`.

```javascript
  // bad
  if (!dragon.age()) {
    return false;
  }

  // good
  if (!dragon.hasAge()) {
    return false;
  }
```



----
## Events

  
 
### 24.1 Event data
  When attaching data payloads to events (whether DOM events or something more proprietary like Backbone events), pass an object literal (also known as a "hash")  instead of a raw value. This allows a subsequent contributor to add more data to the event payload without finding and updating every handler for the event. For example, instead of:

```javascript
  // bad
  $scope.$emit("listingUpdated", listing.id);

  // ...

  $scope.$on("listingUpdated", (e, listingId) => {
    // do something with listingID
  });
```

  prefer:

```javascript
  // good
  $scope.$emit("listingUpdated", { listingId: listing.id });

  // ...

  $scope.$on("listingUpdated", (e, data) => {
    // do something with data.listingId
  });
```
  

----

## Standard Library

  The [Standard Library](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects)
  contains utilities that are functionally broken but remain for legacy reasons.

  
 
### 26.1 NaN checks
  Use `Number.isNaN` instead of global `isNaN`.
  

  > Why? The global `isNaN` coerces non-numbers to numbers, returning true for anything that coerces to NaN.
  > If this behaviour is desired, make it explicit.

```javascript
  // bad
  isNaN("1.2"); // false
  isNaN("1.2.3"); // true

  // good
  Number.isNaN("1.2.3"); // false
  Number.isNaN(Number("1.2.3")); // true
```

  
 
### 26.2 Finite checks
  Use `Number.isFinite` instead of global `isFinite`.
  

  > Why? The global `isFinite` coerces non-numbers to numbers, returning true for anything that coerces to a finite number.
  > If this behaviour is desired, make it explicit.

```javascript
  // bad
  isFinite("2e3"); // true

  // good
  Number.isFinite("2e3"); // false
  Number.isFinite(parseInt("2e3", 10)); // true
```

