## Question ##

How do I actively/efficiently develop while using the `PebblePack` Frontend build process.

I am a spunky Frontend developer who wants to actively make changes to `Plus`, `PebblePadComponents`, .etc. What is the best way for me to develop on them while using the build process?

------
## Answer ##

The Frontend build process is highly configurable, allowing a developer to tweak the build process to suit their current needs. The following guide will cover what commands are available, how best to optimise the build process for active development and highlight any caveats.

## But first... ##

We need to ensure your environment is up to date!

### Repos ###

For each SPA, checkout a branch which supports the PebblePack build process.

### NodeJs ###

Ensure the latest stable version of NodeJs **10.X.X** is installed.

### IIS ###

Update your `Physical path` in IIS for each SPA to point to the `dist` folder instead of `app`.

   For example `C:\pebble\frontend\plus\app` would be changed to `C:\pebble\frontend\plus\dist`.

   To make the changes, open IIS, expand `Sites`, then `Default Web Site`. For each SPA, click on the site in the list and then on the far right click `Basic Settings`. That will launch a prompt which allows you to change the `Physical path`.

   Alternatively the `setuppp.ps1` script could be used again. See the "*Setting up the apps*" section in the [setup guide][2] for where to find it and how to run it.

### NPM ###

If you are not already setup with the private NPM server, do the following:

- In your console execute `npm cache clear --force`

- In your console, execute `npm adduser --registry https://awseu-bld03p.pebblepad.co.uk`

  When prompted for a username and password, use your TeamCity account credentials. This command allows you access to our private `npm` server and the packages on it.

- **For each** Frontend SPA project, in your console navigate to the directory, then execute `npm ci`

   This may take longer than usual due to the cache being nuked. Some patience may be required :)

⁣

### SPA config.js ###

In your SPA's `config.js` file change the return value for  `getSharedComponentBaseUrl` to `https://<my-pc>/<spa-name>/node_modules/@pebblepad/components/`.

Example:
```JavaScript
getSharedComponentBaseUrl: function () {
    return "https://kermit-pc/plus/node_modules/@pebblepad/components/";
},
```

⁣

---

## Commands ##

The Frontend build process relies on JS parsing (ES6 imports, TS), CSS parsing (NodeSCSS compilation, automatic vendor prefixing) and HTML script/style injecting. All of these processes can be run via two commands.

- `npm run build`
- `npm run watch`

### Build ###

The `build` command calls the [PebblePack][1] CLI and tells it to run the npm links (more on that in a bit), scripts, CSS and HTML tasks **once**. By default it uses the production config (`pebblepack.config.json`), produces external sourcemaps and minifies all of the produced files.

This is the same command which the TeamCity build server uses. 

⁣

### Watch ###

The `watch` command calls the same processes as `build`, but allows file caching and incremental builds via file watchers. Essentially this means that a Developer does not have to manually trigger a build every time they make a change and they can benefit from a decreased build time.

⁣

---

## Config ##

As mentioned earlier, the build processes all make use of [PebblePack][1]. PebblePack supports the use of custom configs with a slew of different options. The custom config does not have to be a full config! Any properties you specify will override the original in `pebblepack.config.json` or `PebblePack`'s internal full config. 

For example if your custom config looked like this:

```JSON
{
    "minify": false
}
```

That would override the `"minify": true` in `pebblepack.config.json`, whilst keeping all the other properties!

A full list of config options is available on our [PebblePack npm page][1].

In an attempt to make Developer lives a bit easier, we created a development config for each SPA - `dev.config.json`. This config both serves as an example for custom configs and for general use when working with a SPA and `PebblePadComponents` at the same time). 

To use a custom or the dev config, all you need to do is specify it when running your `watch` or `build` command:
```
npm run watch -- --customConfig=./mySuperAwesomeCustom.config.json
```

> *Note: Specifying the `.json` extension for a custom config is optional. Internally `PebblePack` will check for the `.json` extension and apply it if it is missing.*

⁣

---

## What should I use? ##

For active development, the `watch` command would be the best / fastest way to test changes in a Browser, however using the standard production config will still result in fairly slow build times. A non-production config should be used to further decrease build times!

For SPA and `PebblepadComponents`, most developers should be able to get away with reusing the `dev.config.json` file as the `customConfig` option.

```
npm run watch -- --customConfig=./dev.config.json
```

⁣

### Working on Multiple projects at the same time ###

Often a developer will need to work on a SPA and a library or a monolithic repo (e.g. `PebblepadComponents`). In the *old days* of cowboy coders and ~2s PR reviews, the build process had to guess where a library like `PebblePadComponents` was (relied on it always being in the same parent directory and did a partial name match). This meant that even when working on a SPA in isolation a Developer would have to have the `PebblePadComponents` repo checked out. Moving things to a more modern and sensible era, every dependency of a SPA **must** be a `npm` package.

Fret not dear Developer! Things are not as intimidating as it may seem. When working on a monolithic repo such as `PebblePadComponents` or a private `npm` package, you can tell the build process where your local Development version of the package (e.g. your repo directory) is.

In a `PebblePack` config exists a property called `"localDevelopmentPackages"`. This is a list of JSON objects each with a `"packageName"` and `"path"` property:

```JSON
"localDevelopmentPackages": [
    {
        "packageName": "@pebblepad/components",
        "path": "../pebblepadcomponents"
    }
]
```

You can add entries for any `npm` package you are currently working on at the same time. When the `build` or `watch` processes start, `PebblePack` will automatically link (via `npm link`) and make use of your `"localDevelopmentPackages"`.

For packages which do not have a build process (i.e. `PebblePadComponents`), the SPA build process needs to be aware that it is required to fully process files in that package as well. This is achieved by specifying a `"process"` property in the `PebblePack` config under `js.bundle.bundling.vendor.process`. 

```JSON
"js": {
    "bundling": {
        "vendor": {
            "process": ["@pebblepad/components"]
        }
    }
}
```

*Above example from Plus's pebblepack.config.json*

⁣

---

## Example setup for Plus and PebblePadComponents local development ##

- Update `config.js`'s `getSharedComponentBaseUrl` value
   ```JavaScript
   getSharedComponentBaseUrl: function () {
       //"https://<my-pc>/<spa-name>/node_modules/@pebblepad/components/"
       return "https://kermit-pc/plus/node_modules/@pebblepad/components/";
   },
   ```

- Execute `npm ci` in a console
- Execute `npm run watch -- --customConfig=./dev.config` in a console.

⁣

### Example setup with a **custom** config ###

- Create a custom config. e.g. `kermit-the-frog.config.json`:
   ```JSON
   {
        "js": {
            "hashNames": false
        },
        "cache": true,
        "minify": false,
        "development": true,
        "localDevelopmentPackages": [
            {
                "packageName": "@pebblepad/components",
                "path": "../pebblepadcomponents"
            }
        ]
        
   }
   ```

- Execute `npm ci` in a console
- Execute `npm run watch -- --customConfig=./kermit-the-frog.config.json` in a console.




[1]: https://awseu-bld03p.pebblepad.co.uk/#/detail/@pebblepad/pebblepack
[2]: https://stackoverflow.com/c/pebblepad/questions/204
